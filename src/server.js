const express = require("express");
const http = require("http");
const axios = require('axios');
const socketIo = require("socket.io");
const port = process.env.PORT || 4001;
const app = express();

const router = express.Router();
router.get("/", (req, res) => {
  res.send({ response: "I am alive" }).status(200);
});

app.use(router);
const server = http.createServer(app);
const io = socketIo(server);
io.on("connection", socket => {
  console.log("New client connected"), setInterval(
    () => getApiAndEmit(socket),
    10000
  );
  socket.on("disconnect", () => console.log("Client disconnected"));
});

const getApiAndEmit = async socket => {
  try {
    const res = await axios.get(
      "https://api.darksky.net/forecast/58d8916668299720c5edb3ccf4133e0b/43.7695,11.2558"
    );
    socket.emit("FromAPI", res.data.currently.temperature);
  } catch (error) {
    console.error(`Error: ${error.code}`);
  }
}

server.listen(port, () => console.log(`Listening on port ${port}`));
