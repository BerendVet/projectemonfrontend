import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as io from 'socket.io-client';
import {map, mergeMap, timeInterval, timestamp} from 'rxjs/operators';

const url = 'http://46.101.229.187';

@Injectable({
  providedIn: 'root'
})
export class DataService {


  constructor(private http: HttpClient) {
  }

  public getData(Timestamp: string): Observable<any> {
    return this.http.get(url + '/api/' + Timestamp);
  }
}
