import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {DataService} from '../services/data.service';
import {Chart} from 'chart.js';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {

  _state = 0;
  _Timestamp: any;
  _fromDate: number;
  _toDate: number;
  totalUsage: number;

  @Input() set Timestamp(value: string) {
    this._Timestamp = value;
    this._state = 0;
    this.dataService.getData(eval(`ApiTimestamps.${this._Timestamp}.Call`)).subscribe((data) => {
      this.updateChart(this.LiveDataChart, data, eval(`ApiTimestamps.${this._Timestamp}.Format`));
    });
  }

  @Input() set fromDate(value: number) {
    this._fromDate = value;
    if (this._toDate !== undefined) {
      this._state = 1;
      this.dataService.getData(`getBetween?fromDate=${this._fromDate}&toDate=${this._toDate}&spacing=360`).subscribe((data) => {
        this.updateChart(this.LiveDataChart, data, 'yy-MM-dd HH:mm');
      });
    }
  }

  @Input() set toDate(value: number) {
    this._toDate = value;
    if (this._fromDate !== undefined) {
      this._state = 1;
      this.dataService.getData(`getBetween?fromDate=${this._fromDate}&toDate=${this._toDate}&spacing=360`).subscribe((data) => {
        this.updateChart(this.LiveDataChart, data, 'yy-MM-dd HH:mm');
      });
    }
  }

  colors = ['#ff8f3f', '#ff6a00'];

  LiveDataChart = [];

  constructor(private dataService: DataService, public datePipe: DatePipe) {
  }

  ngOnInit() {
    this.LiveDataChart = new Chart('LiveDataChart', {
      type: 'line',
      options: {
        fill: false,
        maintainAspectRatio: false,
        showLine: true,
        responsive: true,
        legend: {
          display: false
        },
        animation: {
          duration: 0
        },
        scales: {
          xAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Date'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Power consumed (kWh)'
            }
          }]
        }
      }
    });
    setInterval(() => {
      if (this._state == 0) {
        this.dataService.getData(eval(`ApiTimestamps.${this._Timestamp}.Call`)).subscribe((data) => {
          this.updateChart(this.LiveDataChart, data, eval(`ApiTimestamps.${this._Timestamp}.Format`));
        });
      }
    }, 10000);
  }

  updateChart(chart: Chart, data, format) {

    // @ts-ignore
    this.totalUsage = data.length !== 0 ? 
    (data[data.length - 1].elec_in_tarif_1 + data[data.length - 1].elec_in_tarif_2).toFixed(2) + ' kWh' : '0';

    let datasets: [] = data;

    chart.data.labels = datasets.map(e => {
      // @ts-ignore
      return this.datePipe.transform(new Date(e.date), format);
    });
    chart.data.datasets = [{
      borderColor: this.colors[1],
      backgroundColor: this.colors[0],
      data: datasets.map((e: any, i) => {
        return e.act_power_cons;
      })
    }];

    // chart.data.datasets = datasets
    //   .map((e: any, i) => {
    //     console.log(e.date);
    //     let dataset: any = {
    //
    //       data: [{
    //         x: this.datePipe.transform(new Date(e.date), 'dd-HH-mm'),
    //         y: e.act_power_cons,
    //       }]
    //     };
    //
    //     if (options) {
    //       dataset = Object.assign(dataset, options);
    //     }
    //
    //     return dataset;
    //   });

    chart.update();
  }
}

const ApiTimestamps = {
  All: {Call: 'getAll', Format: 'yy-MM-dd'},
  Hour: {Call: 'getLastHour', Format: 'HH:mm'},
  Day: {Call: 'getLastDay', Format: 'HH:mm'},
  Week: {Call: 'getLastWeek', Format: 'MM-dd HH:mm'},
  Month: {Call: 'getLastMonth', Format: 'yy-MM-dd'},
  Year: {Call: 'getLastYear', Format: 'yy-MM-dd'},
};
