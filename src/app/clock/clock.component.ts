import {Component, OnInit} from '@angular/core';
@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})
export class ClockComponent {
  date: Date;

  constructor() {
    setInterval(() => {
      const currentDate = new Date();
      // @ts-ignore
      this.date = currentDate.toLocaleTimeString();
    }, 1000);
  }
}

