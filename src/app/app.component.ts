import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Chart} from 'chart.js';
import {DataService} from './services/data.service';
import {DatePipe} from '@angular/common';
import {MatDatepickerInputEvent, MatSelectChange} from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DataService]
})

export class AppComponent implements OnInit {
  minDate = new Date(2019, 2, 26);
  maxDate = new Date();
  timestamps: string[] = ['All', 'Hour', 'Day', 'Week', 'Month', 'Year'];

  filterdTimestamp = 'All';
  filterdfromDate: number;
  filterdtoDate: number;

  sidenavControl = new FormGroup({
    fromControl: new FormControl(),
    toControl: new FormControl(),
    timestampControl: new FormControl(),
  });

  constructor(private datePipe: DatePipe) {
  }

  private resetFilter() {
    this.sidenavControl.controls.timestampControl.reset();
    this.sidenavControl.controls.fromControl.reset();
    this.sidenavControl.controls.toControl.reset();
  }

  ngOnInit(): void {

  }

  fromFilter(event: MatDatepickerInputEvent<Date>) {
    const dateTime = new Date(event.value);
    // @ts-ignore
    this.filterdfromDate = Math.floor(dateTime / 1000);
  }

  toFilter(event: MatDatepickerInputEvent<Date>) {
    const dateTime = new Date(event.value);
    // @ts-ignore
    this.filterdtoDate = Math.floor(dateTime / 1000);
  }

  changeTimestamp() {
    this.filterdTimestamp = this.sidenavControl.controls.timestampControl.value;
  }
}






